<?php

//include 'config.php';

function listItems($itemDetails, $images)
{
    global $token, $postal_code, $country, $currency;
    $seller_profiles = json_decode($itemDetails['seller_profiles']);
    $price_details = json_decode($itemDetails['price_details']);
    $measurement_details = json_decode($itemDetails['measurement_details']);
    $specifications_details = json_decode($itemDetails['specifications_details']);
    $variation_details = json_decode($itemDetails['variation_details']);
    $postData = '<?xml version="1.0" encoding="utf-8"?>
<AddFixedPriceItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">
  <RequesterCredentials>
    <eBayAuthToken>' . $token . '</eBayAuthToken>
  </RequesterCredentials>
  <ErrorLanguage>en_US</ErrorLanguage>
  <WarningLevel>High</WarningLevel>
  <Item>
    <Title><![CDATA[' . $itemDetails['title'] . ']]></Title>
    <Description>
      <![CDATA[' . $itemDetails['description'] . ']]>
    </Description>
    <PrimaryCategory>
      <CategoryID>' . $itemDetails['category'] . '</CategoryID>
    </PrimaryCategory>
    <StartPrice>' . $price_details->offer_price . '</StartPrice>
    <CategoryMappingAllowed>true</CategoryMappingAllowed>
    <ConditionID>' . $itemDetails['condition'] . '</ConditionID>
    <Country>' . $country . '</Country>
    <Currency>' . $itemDetails['currency'] . '</Currency>
        <Location>' . $itemDetails['location'] . '</Location>
    <DispatchTimeMax>10</DispatchTimeMax>
    <ListingDuration>GTC</ListingDuration>';
    $postData .= '<ProductListingDetails>';
    if (!empty($itemDetails['product_make']) && !empty($itemDetails['product_model'])) {
        $postData .= '<BrandMPN>
    <Brand>' . $itemDetails['product_make'] . '</Brand>
    <MPN>' . $itemDetails['product_model'] . '</MPN>
  </BrandMPN>';
    }
    if (!empty($itemDetails['upc'])) {
        $postData .= '<UPC>' . $itemDetails['upc'] . '</UPC>';
    }
    $postData .= '</ProductListingDetails>';

    if (!empty($specifications_details)) {
        $postData .= '<ItemSpecifics>';
        foreach ($specifications_details as $key => $specification) {
            $postData .= '<NameValueList>
              <Name>' . $key . '</Name>
              <Value>' . $specification . '</Value>
            </NameValueList>';
        }
        $postData .= '</ItemSpecifics>';
    }

    if (!empty($images)) {
        $postData .= '<PictureDetails>';
        foreach ($images as $image) {
            $postData .= '<PictureURL>' . trim($image['image_url']) . '</PictureURL>';
        }
        $postData .= '</PictureDetails>';
    }
    $postData .= '<PostalCode>' . $postal_code . '</PostalCode>';
//    <Site>US</Site>
    if (!empty($variation_details)) {
        $variations_size = array();
        $variations_color = array();
        $variations_array = array();
        foreach ($variation_details->options as $key => $variations) {
            if (trim($variations) == "Size") {
                $variations_array['size'][] = trim($variation_details->values[$key]);
            }

            if (trim($variations) == "Color") {
                $variations_array['color'][] = trim($variation_details->values[$key]);
            }

            if (trim($variations) == "SKU") {
                $variations_array['sku'][] = trim($variation_details->values[$key]);
            }

            if (trim($variations) == "Image") {
                $variations_array['image'][] = trim($variation_details->values[$key]);
            }

            if (trim($variations) == "Quantity") {
                $variations_array['quantity'][] = trim($variation_details->values[$key]);
            }

            if (trim($variations) == "Price") {
                $variations_array['price'][] = trim($variation_details->values[$key]);
            }
        }
        $vairationImage = "";
        $variations = "";
        $VariationSpecificsSetColor = "";
        $VariationSpecificsSetSize = "";
        if (!empty($variations_array['sku'])) {
            $sumQuantity = 0;
            foreach ($variations_array['sku'] as $key => $sku) {
//                $postData .= '<Variation>';
                $VariationSpecifics = "";
                $variations .= '<Variation> <SKU>' . $sku . '</SKU>
        <StartPrice>' . $variations_array['price'][$key] . '</StartPrice>
        <Quantity>' . $variations_array['quantity'][$key] . '</Quantity>';
                $sumQuantity = $sumQuantity + (int)$variations_array['quantity'][$key];
                $VariationSpecifics .= '<VariationSpecifics>';
                if (!empty($variations_array['color'][$key])) {
                    $VariationSpecificsSetColor .= '<Value>' . ucfirst($variations_array['color'][$key]) . '</Value>';
                    $VariationSpecifics .= '<NameValueList><Name>Color</Name><Value>' . $variations_array['color'][$key] . '</Value></NameValueList>';
                    if (!empty($variations_array['image'][$key])) {
                        $vairationImage .= '<VariationSpecificPictureSet>
          <VariationSpecificValue>' . ucfirst($variations_array['color'][$key]) . '</VariationSpecificValue>
          <PictureURL>' . $variations_array['image'][$key] . '</PictureURL>
        </VariationSpecificPictureSet>';
                    }
                }

                if (!empty($variations_array['size'][$key])) {
                    $VariationSpecifics .= '<NameValueList><Name>Size</Name><Value>' . $variations_array['size'][$key] . '</Value></NameValueList>';
                    $VariationSpecificsSetSize .= '<Value>' . ucwords($variations_array['size'][$key]) . '</Value>';
                }
                $VariationSpecifics .= '</VariationSpecifics>';
                $variations .= $VariationSpecifics . '</Variation>';
            }


            if (!empty($variations)) {
                $postData .= '<Quantity>' . $sumQuantity . '</Quantity>';
                $postData .= "<Variations>";

                if (!empty($VariationSpecificsSetSize) || !empty($VariationSpecificsSetColor)) {
                    $postData .= "<VariationSpecificsSet>";
                    if (!empty($VariationSpecificsSetSize)) {
                        $postData .= "<NameValueList><Name>Size</Name>" . $VariationSpecificsSetSize . '</NameValueList>';
                    }

                    if (!empty($VariationSpecificsSetColor)) {
                        $postData .= "<NameValueList><Name>Color</Name>" . $VariationSpecificsSetColor . '</NameValueList>';
                    }
                    $postData .= "</VariationSpecificsSet>";
                }
                $postData .= $variations;
            }
        }
        if (!empty($vairationImage)) {
            $postData .= '<Pictures><VariationSpecificName>Color</VariationSpecificName>' . trim($vairationImage) . '</Pictures>';
        }
        if (!empty($variations)) {
            $postData .= '</Variations>';
        }
    }
    $postData .= '<SellerProfiles>';
    foreach ($seller_profiles as $key => $profile) {
        if ($key == 'payment_profile_name') {
            $postData .= '<SellerPaymentProfile>
        <PaymentProfileName>' . trim($profile) . '</PaymentProfileName>
      </SellerPaymentProfile>';
        }

        if ($key == 'retrun_profile_name') {
            $postData .= ' <SellerReturnProfile>
        <ReturnProfileName>' . trim($profile) . '</ReturnProfileName>
      </SellerReturnProfile>';
        }

        if ($key == 'shipping_profile_name') {
            $postData .= ' <SellerShippingProfile>
        <ShippingProfileName>' . trim($profile) . '</ShippingProfileName>
      </SellerShippingProfile>';
        }
    }
    $postData .= '</SellerProfiles>
  </Item>
</AddFixedPriceItemRequest>';

    echo $postData;
    die;
    $reults = callEbayAPI($postData, "AddFixedPriceItem");
    return $reults;
}

function callEbayAPI($postData, $callName)
{
    global $developer_id, $application_id, $certificate_id, $compability_level, $site_id, $api_url;
    $header = array(
        "X-EBAY-API-COMPATIBILITY-LEVEL: $compability_level",
        "X-EBAY-API-DEV-NAME: $developer_id",
        "X-EBAY-API-APP-NAME: $application_id",
        "X-EBAY-API-CERT-NAME: $certificate_id",
        "X-EBAY-API-SITEID: $site_id",
        "X-EBAY-API-CALL-NAME: " . $callName,
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $api_url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $results = curl_exec($ch);
    $error = curl_error($ch);
    curl_close($ch);
    return $results;
}

/*
 * 
 */