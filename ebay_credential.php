<?php
require_once "db/AppManager.php";
$pm = AppManager::getPM();
$sql = "SELECT * FROM ebay_store WHERE customer_id=1 LIMIT 1";
$store = $pm->fetchResult($sql);
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <style>
            .btn{
                color: #fff !important;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <br>
            <h1>eBay Store</h1>
            <br>
            <form method="post">
                <div class="form-group">
                    <label class="control-label">Store Site Id</label>
                    <select name="site_id" class="form-control">
                        <option <?php echo (!empty($store) && $store[0]['site_id'] == "" ? 'selected' : ''); ?> value="">Select eBay Site Id</option>
                        <option <?php echo (!empty($store) && $store[0]['site_id'] == 0 ? 'selected' : ''); ?> value="0">eBay United States</option>
                        <option <?php echo (!empty($store) && $store[0]['site_id'] == 2 ? 'selected' : ''); ?> value="2">eBay Canada (English)</option>
                        <option <?php echo (!empty($store) && $store[0]['site_id'] == 3 ? 'selected' : ''); ?> value="3">eBay UK</option>
                        <option <?php echo (!empty($store) && $store[0]['site_id'] == 15 ? 'selected' : ''); ?> value="15">eBay Australia</option>
                        <option <?php echo (!empty($store) && $store[0]['site_id'] == 16 ? 'selected' : ''); ?> value="16">eBay Austria</option>
                        <option <?php echo (!empty($store) && $store[0]['site_id'] == 23 ? 'selected' : ''); ?> value="23">eBay Belgium (French)</option>
                        <option <?php echo (!empty($store) && $store[0]['site_id'] == 71 ? 'selected' : ''); ?> value="71">eBay France</option>
                        <option <?php echo (!empty($store) && $store[0]['site_id'] == 77 ? 'selected' : ''); ?> value="77">eBay Germany</option>
                        <option <?php echo (!empty($store) && $store[0]['site_id'] == 100 ? 'selected' : ''); ?> value="100">EBAY-MOTOR</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Application ID</label>
                    <input type="text" class="form-control" name="application_id" value="<?php echo(!empty($store) && $store[0]['application_id'] ? $store[0]['application_id'] : ''); ?>">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Certificate ID</label>
                    <input type="text" class="form-control" name="certificate_id" value="<?php echo (!empty($store) && $store[0]['certificate_id'] ? $store[0]['certificate_id'] : ''); ?>">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Developer ID</label>
                    <input type="text" class="form-control" name="developer_id" value="<?php echo (!empty($store) && $store[0]['developer_id'] ? $store[0]['developer_id'] : ''); ?>">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Compatability Level</label>
                    <input type="text" class="form-control" name="compability_level" value="<?php echo (!empty($store) && $store[0]['compability_level'] ? $store[0]['compability_level'] : ''); ?>">
                    <input type="hidden" class="form-control" name="id" value="<?php echo (!empty($store) && $store[0]['id'] ? $store[0]['id'] : ''); ?>">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Token</label>
                    <textarea name="token" class="form-control"><?php echo(!empty($store) && $store[0]['token'] ? $store[0]['token'] : ''); ?></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
        <?php
        $postData = (isset($_POST) ? $_POST : '');
        if (!empty($postData)) {
            if (!empty($postData['id'])) {
                $sql = "UPDATE `ebay_store` SET site_id='" . $postData['site_id'] . "', application_id='" . $postData['application_id'] . "', certificate_id='" . $postData['certificate_id'] . "' , developer_id='" . $postData['developer_id'] . "', compability_level='" . $postData['compability_level'] . "', token='" . $postData['token'] . "'";
                $pm->executeQuery($sql);
                header("Location: ebay_credential.php");
            } else {
                $sql = "INSERT INTO `ebay_store` (`id`, `site_id`, `application_id`, `certificate_id`, `developer_id`, `compability_level`, `token`, `customer_id`) VALUES (NULL, '" . $postData['site_id'] . "', '" . $postData['application_id'] . "', '" . $postData['certificate_id'] . "', '" . $postData['developer_id'] . "', '" . $postData['compability_level'] . "', '" . $postData['token'] . "', 1)";
                $pm->executeQuery($sql);
            }
        }
        ?>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </body>
</html>