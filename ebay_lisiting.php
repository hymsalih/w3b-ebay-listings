<?php

session_start();
if (empty($_SESSION["customer_id"])) {
    echo "\nCustomer id not found";
    die;
}
$customer_id = $_SESSION["customer_id"];


require_once('db/DatabaseManager.php');
$pm = new DatabaseManager();
require_once('ebayCallAPI.php');
error_reporting(E_ALL);
set_time_limit(0);
$items = $pm->fetchResult("SELECT products.*,seller_profiles.details as seller_profiles, item_costs.price_details,item_measurements.measurement_details,item_specifications.specifications_details,item_variation.variation_details
FROM products
LEFT JOIN item_costs ON products.id = item_costs.item_id
LEFT JOIN item_measurements ON products.id = item_measurements.item_id
LEFT JOIN seller_profiles ON products.id = seller_profiles.item_id
LEFT JOIN item_specifications ON products.id = item_specifications.item_id
LEFT JOIN item_variation ON products.id = item_variation.item_id WHERE products.listing_status ='pending'");
$total_success_listings = $total_error_listings = 0;
foreach ($items as $itemDetails) {
    $images = $pm->fetchResult("SELECT * FROM item_images WHERE item_id='" . $itemDetails['id'] . "'");
    $response = listItems($itemDetails, $images);
    try {
        $response = new SimpleXMLElement($response);
        if ($response->Ack == 'Success' || $response->Ack == 'Warning') {
            $item_id = (string)$response->ItemID;
            $sql = "UPDATE products SET listing_status='listed', market_place_item_id='" . $item_id . "' WHERE id='" . $itemDetails['id'] . "'";
            $pm->executeQuery($sql);
            $total_success_listings++;
        } else {
            $sql = "UPDATE products SET ebay_listing_status='error', response='" . addslashes(json_encode($response)) . "' WHERE id='" . $itemDetails['id'] . "'";
            $pm->executeQuery($sql);
            $errors = json_encode($response->Errors);
            $sql = "INSERT INTO `api_response` (`id`, `call_name`, `item_id`, `response`) VALUES (NULL, 'CreateListings', '" . $new_listing_product['id'] . "', '" . addslashes($errors) . "')";
            $pm->executeQuery($sql);
            $total_error_listings++;
        }
    } catch (Exception $ex) {
        print_r($ex);
        die;
    }
}

echo "\nTotal success listings :- " . $total_success_listings;
echo "\nTotal variation error listings :- " . $total_error_listings;