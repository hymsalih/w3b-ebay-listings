<?php

session_start();
if (empty($_SESSION["customer_id"])) {
    echo "\nCustomer id not found";
    die;
}
$customer_id = $_SESSION["customer_id"];
error_reporting(E_ALL);
set_time_limit(0);
ini_set('memory_limit', '512M');

date_default_timezone_set('Europe/London');
set_include_path(get_include_path() . PATH_SEPARATOR . 'PHPExcel/Classes/');

/** PHPExcel_IOFactory */
include 'PHPExcel/IOFactory.php';
require_once('db/DatabaseManager.php');
$pm = new DatabaseManager();

//$inputFileType = 'Excel5';
$inputFileType = 'Excel2007';
//	$inputFileType = 'Excel2003XML';
//	$inputFileType = 'OOCalc';
//	$inputFileType = 'Gnumeric';

if (!empty($_FILES)) {
    $inputFileName = 'product_template.xlsx';
    $tmp_name = $_FILES["eBayListingTemplate"]["tmp_name"];
    move_uploaded_file($tmp_name, $inputFileName);
} else {
    echo "Invalid uploads";
    die;
}
//$sheetname = 'BOYS NECK TIES';

include 'MyReadFilter.php';


$filterSubset = new MyReadFilter();
$objReader = PHPExcel_IOFactory::createReader($inputFileType);
$worksheetNames = $objReader->listWorksheetNames($inputFileName);
$objReader->setLoadAllSheets();
$objPHPExcel = $objReader->load($inputFileName);
$count = 0;
$uploadingItems = array();


$condition_array = array(
    "" => '',
    "New" => '1000',
    "New other (see details)" => '1500',
    "New with defects" => '1750',
    "Manufacturer refurbished" => '2000',
    "Seller refurbished" => '2500',
    "Like New" => '2750',
    "Used" => '3000',
    "Very Good" => '4000',
    "Good" => '5000',
    "Acceptable" => '6000',
    "For parts or not working" => '7000'
);

for ($sheet = 0; $sheet < count($worksheetNames); $sheet++) {
    $count++;
    $objPHPExcel->setActiveSheetIndex($sheet);
    $activeSheet = $objPHPExcel->getActiveSheet();
    $lastvaluefound = 1;
    $i = 1;
    foreach ($activeSheet->getRowIterator() as $row) {
        if ($i > 1) {
            $sku = $objPHPExcel->getActiveSheet()->getCell('A' . $i)->getValue();
            $upc = $objPHPExcel->getActiveSheet()->getCell('B' . $i)->getValue();
            $title = $objPHPExcel->getActiveSheet()->getCell('C' . $i)->getValue();
            $category = $objPHPExcel->getActiveSheet()->getCell('D' . $i)->getValue();
            $condition = $objPHPExcel->getActiveSheet()->getCell('E' . $i)->getValue();
            $condition = $condition_array[trim($condition)];
            $currency = $objPHPExcel->getActiveSheet()->getCell('F' . $i)->getValue();
            $comparePrice = $objPHPExcel->getActiveSheet()->getCell('G' . $i)->getValue();
            $offerPrice = $objPHPExcel->getActiveSheet()->getCell('H' . $i)->getValue();
            $unitCost = $objPHPExcel->getActiveSheet()->getCell('I' . $i)->getValue();
            $taxExcempt = $objPHPExcel->getActiveSheet()->getCell('J' . $i)->getValue();
            $weight = $objPHPExcel->getActiveSheet()->getCell('K' . $i)->getValue();
            $qtyStock = $objPHPExcel->getActiveSheet()->getCell('L' . $i)->getValue();
            $lowThreshhold = $objPHPExcel->getActiveSheet()->getCell('M' . $i)->getValue();
            $wareHouseLocation = $objPHPExcel->getActiveSheet()->getCell('N' . $i)->getValue();
            $description = $objPHPExcel->getActiveSheet()->getCell('O' . $i)->getValue();
            $variationsOptions = $objPHPExcel->getActiveSheet()->getCell('P' . $i)->getValue();
            $variationsValues = $objPHPExcel->getActiveSheet()->getCell('Q' . $i)->getValue();
            $warehouseBin = $objPHPExcel->getActiveSheet()->getCell('R' . $i)->getValue();
            $height = $objPHPExcel->getActiveSheet()->getCell('S' . $i)->getValue();
            $length = $objPHPExcel->getActiveSheet()->getCell('T' . $i)->getValue();
            $width = $objPHPExcel->getActiveSheet()->getCell('U' . $i)->getValue();
            $images = $objPHPExcel->getActiveSheet()->getCell('V' . $i)->getValue();
            $productMake = $objPHPExcel->getActiveSheet()->getCell('W' . $i)->getValue();
            $productModel = $objPHPExcel->getActiveSheet()->getCell('X' . $i)->getValue();
            $itemSpecifications = $objPHPExcel->getActiveSheet()->getCell('Y' . $i)->getValue();
            $location = $objPHPExcel->getActiveSheet()->getCell('Z' . $i)->getValue();
            $paymentProfileName = $objPHPExcel->getActiveSheet()->getCell('AA' . $i)->getValue();
            $returnsProfileName = $objPHPExcel->getActiveSheet()->getCell('AB' . $i)->getValue();
            $shippingProfileName = $objPHPExcel->getActiveSheet()->getCell('AC' . $i)->getValue();
            $shipToCustomer = $objPHPExcel->getActiveSheet()->getCell('AD' . $i)->getValue();
            $pickStoreIn = $objPHPExcel->getActiveSheet()->getCell('AE' . $i)->getValue();
            $chargeShipping = $objPHPExcel->getActiveSheet()->getCell('AF' . $i)->getValue();
            $commission = $objPHPExcel->getActiveSheet()->getCell('AG' . $i)->getValue();
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);
            $skus[] = $sku;
            $uploadingItems[$sku]['products'] = array('location' => $location, 'sku' => $sku, 'upc' => $upc, 'product_title' => $title, 'condition' => $condition, 'currency' => $currency, 'category' => $category, 'quantity' => $qtyStock, 'ware_house_location' => $wareHouseLocation, 'description' => $description, 'images' => $images, 'product_make' => $productMake, 'product_model' => $productModel, 'ship_to_customer' => $shipToCustomer, 'pick_in_store' => $pickStoreIn);
            $itemSpecifications = explode(";", $itemSpecifications);
            $itemSpecifications = array_filter($itemSpecifications);
            $itemSpecifications = array_unique($itemSpecifications);
            $item_specifications_array = array();
            foreach ($itemSpecifications as $itemSpecification) {
                $arr = explode("=", $itemSpecification, 2);
                if (!empty($arr)) {
                    $item_specifications_array[trim($arr[0])] = trim($arr[1]);
                }
            }
            $uploadingItems[$sku]['products']['prices'] = array('compare_price' => $comparePrice, 'offer_price' => $offerPrice, 'unit_cost' => $unitCost, 'tax_excempt' => $taxExcempt, 'low_thresh_hold' => $lowThreshhold, 'charge_shipping_by_weight' => $chargeShipping, 'commission' => $commission);
            $uploadingItems[$sku]['products']['measurements'] = array('weight' => $weight, 'height' => $height, 'length' => $length, 'width' => $width);
            $uploadingItems[$sku]['products']['seller_profiles'] = array('payment_profile_name' => $paymentProfileName, 'retrun_profile_name' => $returnsProfileName, 'shipping_profile_name' => $shippingProfileName);
            $uploadingItems[$sku]['products']['item_specifications'] = $item_specifications_array;
            $variationsOptions = explode(",", $variationsOptions);
            if (count($variationsOptions) > 1) {
                foreach ($variationsOptions as $variationsOption) {
                    if (!empty($variationsOption)) {
                        $uploadingItems[$sku]['variations']['options'][] = $variationsOption;
                    }
                }
            } else {
                $uploadingItems[$sku]['variations']['options'][] = $variationsOptions[0];
            }
            $variationsValues = explode(",", $variationsValues);
            if (count($variationsValues) > 1) {
                foreach ($variationsValues as $variationsValue) {
                    if (!empty($variationsValue)) {
                        $uploadingItems[$sku]['variations']['values'][] = $variationsValue;
                    }
                }
            } else {
                $uploadingItems[$sku]['variations']['values'][] = $variationsOptions[0];
            }
        }
        $i++;
    }
}
//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $inputFileType);
//$objWriter->save("vests_new.xlsx");

foreach ($uploadingItems as $uploadingItem) {
    $sql = "SELECT COUNT(*) as c FROM products WHERE sku='" . $uploadingItem['products']['sku'] . "' AND customer_id='" . $customer_id . "' AND market_place='eBay' ";
    $total_products = $pm->getCount($sql);
    if ($total_products == 0 && !empty($uploadingItem['products']['sku'])) {
        $sql = "INSERT INTO `products` (`id`, `customer_id`, `market_place`, `title`, `upc`, `sku`, `category`,  "
            . " `quantity`,  `ware_house_location`, `description`,"
            . " `product_make`, `product_model`, `ship_to_customer`, `pick_in_store`, "
            . " `listing_status`,`condition`,`location`,`currency`) VALUES (NULL, '" . $customer_id . "' ,'eBay' ,"
            . "'" . addslashes($uploadingItem['products']['product_title']) . "', '" . addslashes($uploadingItem['products']['upc']) . "', '" . addslashes($uploadingItem['products']['sku']) . "','" . addslashes($uploadingItem['products']['category']) . "', '" . addslashes($uploadingItem['products']['quantity']) . "', '" . addslashes($uploadingItem['products']['ware_house_location']) . "', '" . addslashes($uploadingItem['products']['description']) . "', "
            . "'" . addslashes($uploadingItem['products']['product_make']) . "', '" . addslashes($uploadingItem['products']['product_model']) . "',"
            . " '" . addslashes($uploadingItem['products']['ship_to_customer']) . "', '" . addslashes($uploadingItem['products']['pick_in_store']) . "','pending','" . $uploadingItem['products']['condition'] . "','" . $uploadingItem['products']['location'] . "','" . $uploadingItem['products']['currency'] . "')";
        $exe = $pm->executeQuery($sql);
        if ($exe > 0) {
            $sql = "SELECT LAST_INSERT_ID() AS id";
            $last_inserted_id = $pm->fetchResult($sql);
            if (!empty($last_inserted_id)) {
                /*
                 *   insert images
                 */
                $product_id = $last_inserted_id[0]['id'];
                $images = $uploadingItem['products']['images'];
                $images = explode(",", $images);
                foreach ($images as $image) {
                    $sql = "INSERT INTO `item_images` (`id`, `item_id`, `image_url`) VALUES (NULL, '" . $last_inserted_id[0]['id'] . "', '$image')";
                    $pm->executeQuery($sql);
                }

                /*
                 * insert prices
                 */
                $prices_array = json_encode($uploadingItem['products']['prices']);
                $sql = "INSERT INTO `item_costs` (`id`, `item_id`, `price_details`) VALUES (NULL, '" . $product_id . "', '" . $prices_array . "');";
                $pm->executeQuery($sql);

                /*
                 * insert measurements
                 */
                $measurements_array = json_encode($uploadingItem['products']['measurements']);
                $sql = "INSERT INTO `item_measurements` (`id`, `item_id`, `measurement_details`) VALUES (NULL, '" . $product_id . "', '" . $measurements_array . "')";
                $pm->executeQuery($sql);

                /*
                * insert seller profiles
                */
                if (!empty($uploadingItem['products']['seller_profiles'])) {
                    $seller_profiles = json_encode($uploadingItem['products']['seller_profiles']);
                    $sql = "INSERT INTO `seller_profiles` (`id`, `item_id`, `details`) VALUES (NULL, '" . $last_inserted_id[0]['id'] . "', '" . $seller_profiles . "')";
                    $pm->executeQuery($sql);
                }

                /*
                 * item specifications
                 */
                $item_specifications = json_encode($uploadingItem['products']['item_specifications']);
                $sql = "INSERT INTO `item_specifications` (`id`, `item_id`, `specifications_details`) VALUES (NULL, '" . $product_id . "', '" . $item_specifications . "')";
                $pm->executeQuery($sql);

                /*
                 * insert variation details
                 */
                if (!empty($uploadingItem['variations']) && $uploadingItem['variations']['options']) {
                    $variations = json_encode($uploadingItem['variations']);
                    $sql = "INSERT INTO `item_variation` (`id`, `item_id`, `variation_details`) VALUES (NULL, '" . $product_id . "', '" . $variations . "');";
                    $pm->executeQuery($sql);
                }

            }
        }
    }
}

$pm->closeConnection();

/*
  TRUNCATE item_costs;
  TRUNCATE item_measurements;
  TRUNCATE item_specifications;
  TRUNCATE item_variation;
  TRUNCATE products;
  TRUNCATE api_response;
  TRUNCATE item_costs;
  TRUNCATE item_images;
  TRUNCATE listing_api_response;
  TRUNCATE seller_profiles;
  TRUNCATE item_more_details;

 */
header("Location: index.php");
?>
