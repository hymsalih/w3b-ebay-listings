<?php
require_once "db/DatabaseManager.php";
$pm = new DatabaseManager();
$items = $pm->fetchResult("SELECT products.*, item_costs.* FROM products LEFT JOIN item_costs ON products.id = item_costs.item_id ORDER BY products.id DESC");
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        .btn {
            color: #fff !important;
        }
    </style>
</head>
<body>
<div class="container">
    <!--<h1>Listing eBay</h1>-->
    <br>
    <form action="upload_items.php" method="post" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <input type="hidden" name="action" value="import_template">
                    <label for="exampleFormControlTextarea1">Upload EXCEL</label>
                    <input type="file" required name="eBayListingTemplate" class="form-control"
                           accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <button class="btn btn-sm btn-primary btn-block" type="submit">Upload</button>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <a href="ebay_lisiting.php" class="btn btn-sm btn-success btn-block">List Items</a>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <a href="ebay_credential.php" class="btn btn-sm btn-danger btn-block">eBay Store</a>
                </div>
            </div>
        </div>
    </form>
    <h1>Items</h1>
    <br>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>UPC</th>
            <th>TITLE</th>
            <th>ITEM ID</th>
            <th>PRICE</th>
            <th>QUANTITY</th>
            <th>STATUS</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($items as $item) {
            $price = json_decode($item['price_details']);
            ?>
            <tr>
                <td><?php echo $item['upc'] ?></td>
                <td><?php echo $item['title'] ?></td>
                <td><?php echo $item['sku'] ?></td>
                <td><?php echo $price->offer_price; ?></td>
                <td><?php echo $item['quantity'] ?></td>
                <td>
                    <?php
                    if ($item['listing_status'] == "pending") {
                        echo '<a class="btn btn-danger no-link">Pending</a>';
                    } else if ($item['listing_status'] == "listed") {
                        echo '<a class="btn btn-success no-link">Listed</a>';
                        echo '<br><a href="https://www.ebay.com/itm/' . $item['market_place_item_id'] . '">View item</a>';
                    } else if ($item['listing_status'] == "error_in_listing") {
                        echo '<a class="btn btn-danger no-link">Error In Listing</a>';
                        echo '<br><a href="error.php?id=' . $item['id'] . '">View error details</a>';
                    }
                    ?>
                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</body>
</html>